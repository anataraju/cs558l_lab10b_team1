#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <pthread.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <pcap.h>
#include <netinet/if_ether.h>
#include <netinet/ip_icmp.h>
#include <sys/ioctl.h>        // macro ioctl is defined
#include <bits/ioctls.h>      // defines values for argument "request" of ioctl.
#include <net/if.h>           // struct ifreq
#include <unistd.h>           // close()
#include <netinet/ether.h>
#include <net/ethernet.h>
#include <time.h>
#include <sys/time.h>
#include "common.h"
#include "udp_receiver.h"

int numPktsRecvd = 0;
int isFirstPkt = 0;
struct timeval start, end;
int conn_state;
int totalPackets = 0;
int packet_size = 100;
unsigned char udpr_src_mac[6];
pcap_t *whandle;

void udp_receiver(session_t *sess)
{
	dprintf("In udpReceiver\n");

	//Receiver should keep listening
	char dev[MAX_NAME_LENGTH];
	char errbuf[PCAP_ERRBUF_SIZE];	/* Error string */

	strcpy(dev, "inf000");

	memset(udpr_src_mac, 0, 6);

	populateMAC(dev, udpr_src_mac);

	dprintf("device name is %s, MAC address is",dev);
	print_mac_address(udpr_src_mac);

	//open a write handle
	/* open capture device */
	whandle = pcap_open_live(dev, SNAP_LEN, 1, 1000, errbuf);
	if (whandle == NULL) {
		fprintf(stderr, "Couldn't open device %s: %s\n", dev, errbuf);
		exit(EXIT_FAILURE);
	}


	//Fetch the 
	conn_state = IDLE_STATE;

	//Call Packet sniffer to see if there is some packet
	packetSniffer(dev, sess);
	pcap_close(whandle);
}


void packetSniffer(char *dev, session_t *sess)
{
	dprintf("In packetSniffer\n");
	/* print capture info */
	dprintf("Device: %s\n", dev);
	pcap_t *handle;
	char errbuf[PCAP_ERRBUF_SIZE];	/* Error string */

	char getMac[MAX_NAME_LENGTH];
	char filter_exp[] = "ether src ";		/* filter expression [3] */
	getMacForPcapFilter(sess->senderName,getMac,sess);
	strcat(filter_exp,getMac);
	dprintf ("Filter expression:%s\n",filter_exp);

	struct bpf_program fp;			/* compiled filter program (expression) */
	bpf_u_int32 mask;			/* subnet mask */
	bpf_u_int32 net;			/* ip */

	/* get network number and mask associated with capture device */
	if (pcap_lookupnet(dev, &net, &mask, errbuf) == -1) {
		fprintf(stderr, "Couldn't get netmask for device %s: %s\n",
				dev, errbuf);
		net = 0;
		mask = 0;
	}

	/* open capture device */
	handle = pcap_open_live(dev, SNAP_LEN, 1, 1000, errbuf);
	if (handle == NULL) {
		fprintf(stderr, "Couldn't open device %s: %s\n", dev, errbuf);
		exit(EXIT_FAILURE);
	}

	/* compile the filter expression */
	if (pcap_compile(handle, &fp, filter_exp, 0, net) == -1) {
		fprintf(stderr, "Couldn't parse filter %s: %s\n",
				filter_exp, pcap_geterr(handle));
		exit(EXIT_FAILURE);
	}

	/* apply the compiled filter */
	if (pcap_setfilter(handle, &fp) == -1) {
		fprintf(stderr, "Couldn't install filter %s: %s\n",
				filter_exp, pcap_geterr(handle));
		exit(EXIT_FAILURE);
	}


	//call pcap_loop()
	while(pcap_loop(handle,-1, my_callback, (u_char *)sess) != -1);


}

void my_callback(u_char *pses, const struct pcap_pkthdr* pkthdr,const u_char* packet)
{
	session_t * pSession = (session_t *) pses;

#ifdef DEBUG
	static int count = 1;
#endif
	pkt_h *hdr;

	const char *payload;                    /* Packet payload */

	dprintf("In my_callback: Packet number [%d], length of this packet is: %d\n", count++, pkthdr->len);

	hdr = (pkt_h *) packet;

	dprintf("Length of Packet: %d\n", pkthdr->len);
	dprintf("Host Address: %d\n", HOST_PART(hdr));
	dprintf("Network Address: %d\n", NW_PART(hdr));
	dprintf("Type of Packet: %d\n", PACKET_TYPE(hdr));
	dprintf("conn state is : %d\n", conn_state);

	//If IDLE state, wait for SYN packet
	if(conn_state == IDLE_STATE)
	{
		//check if it is SYN packet
		if(IS_SYN(hdr))
		{
			dprintf("SYN bit is set\n");

			//Start the packet counter
			//numPktsRecvd++;

			//Get the start time
			gettimeofday(&start, NULL);

			//generate a SYN_ACK return

			//change the connection state
			conn_state = ESTABLISHED;

			//return

			//Send SYN_ACKs
			unsigned char *pkt=NULL;
			int i=0;
			unsigned int pkt_size=0;
			if (pSession->packetSize == TYPE0_SIZE )
				pkt_size=udp_make_packet(/*packt type */0, /*FIN*/0, /*SYN*/1, /*ACK*/1, pSession->numPktsSent, &pkt, hdr->ether_shost, udpr_src_mac);
			else
				pkt_size=udp_make_packet(/*packt type */1, /*FIN*/0, /*SYN*/1, /*ACK*/1, pSession->numPktsSent, &pkt, hdr->ether_shost, udpr_src_mac);


			for(i=0;i<NO_OF_DUP;i++)
			{
				if (pcap_inject(whandle,pkt,pkt_size)==-1)
				{
					pcap_perror(whandle,0);
					pcap_close(whandle);
					exit(1);
				}
			}
			free(pkt);

		}
		else
		{
			//Do not receive any packets
			return;
		}
	}
	else if(conn_state == ESTABLISHED)
	{


		dprintf("about to set is FIN bit\n");
		//check if it is FIN packet
		if(IS_FIN(hdr))
		{
			dprintf("FIN bit is set\n");

			//Retrieve the int count from payload
			/* define/compute tcp payload (segment) offset */
			payload = (char *) ((char *)hdr + sizeof(pkt_h));
			memcpy(&totalPackets, (char *)payload, sizeof(int));

			if(IS_LARGE_PKT(hdr))
			{
				packet_size = TYPE1_SIZE;
			}

			// print the statistics
			printStatistics();

			//Reset everything now
			numPktsRecvd = 0;
			isFirstPkt = 0;
			totalPackets = 0;
			packet_size = 100;

			//clear the connection state
			conn_state = IDLE_STATE;

			//comment below
			exit(0);

			return;
		}
		//check if it is SYN packet
		else if(IS_SYN(hdr))
		{
			//ignore it
			return;
		}

		numPktsRecvd++;

#ifdef PRINT_PAYLOAD
		/* define/compute tcp payload (segment) offset */
		payload = (char *) ((char *)hdr + sizeof(pkt_h));

		/* compute tcp payload (segment) size */
		size_payload = pkthdr->len - sizeof(pkt_h);

		/*
		 * Print payload data; it might be binary, so don't just
		 * treat it as a string.
		 */
		if (size_payload > 0) {
			dprintf("   Payload (%d bytes):\n", size_payload);
			print_payload((u_char *)payload, size_payload);
			exit(0);
		}
#endif



	}
}

void printStatistics()
{
	dprintf("In printStatistics\n");
	long elapsed_usec;
	double elapsed_sec;
	double throughput;
	long transfer_bytes;
	double transfer_mbytes;
	int lost = totalPackets - numPktsRecvd;
	int lossPercent = (double)((double)lost / (double)totalPackets) * 100;
	long packetsPerSec = 0;

	dprintf("reached here\n");

	//calculate end time
	gettimeofday(&end, NULL);



	//calculate time elapsed
	elapsed_usec = (end.tv_usec - start.tv_usec) + SECTOUSEC * ( end.tv_sec - start.tv_sec );
	elapsed_sec = (double) elapsed_usec / (double) SECTOUSEC;


	//Total packets received
	throughput = ((numPktsRecvd * packet_size * 8) / elapsed_sec) / SECTOUSEC;
	packetsPerSec = numPktsRecvd / elapsed_sec;

	transfer_bytes =  numPktsRecvd * packet_size;
	transfer_mbytes = transfer_bytes / SECTOUSEC;

	printf("Interval\tTransfer         Bandwidth\tLost\tTotal Datagrams\tPackets/sec\n");
	printf("%.01lf sec\t%.02lf MBytes\t%.02lf Mbits/sec\t%d\t%d (%d)\t%ld\n", elapsed_sec, transfer_mbytes, throughput, lost, totalPackets, lossPercent, packetsPerSec);

}
