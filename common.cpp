#include <stdio.h>
#include "common.h"
#include <stdint.h>
#include <string.h>
#include <netinet/if_ether.h>
#include <netinet/ip_icmp.h>
#include <sys/ioctl.h>        // macro ioctl is defined
#include <bits/ioctls.h>      // defines values for argument "request" of ioctl.
#include <net/if.h>           // struct ifreq
#include <unistd.h>           // close()
#include <netinet/ether.h>
#include <net/ethernet.h>

void print_udp_packet(unsigned char *pkt)
{

	pkt_h *ptr_to_head=NULL;
	ptr_to_head =(pkt_h *)pkt;


	dprintf("Packet generated contents\n");
	dprintf("First byte:%d\n",ptr_to_head->first);
	dprintf("src_mac:");
	print_mac_address(ptr_to_head->ether_shost);
	dprintf("dst_mac:");
	print_mac_address(ptr_to_head->ether_dhost);
	dprintf("Payload data is : %s\n",(char *) (pkt+sizeof(pkt_h)));


	return;
}

void print_received_parm(uint8_t host,uint8_t net,uint8_t pkt_type,uint8_t ack, uint8_t syn,uint8_t fin,unsigned int num_of_pkts)
{
	fprintf(stdout,"host %d\nnet %d\npkt_type %d\nack %d\nsyn %d\nfin %d\nnum_of_pkts %u\n",host,net,pkt_type,ack,syn,fin,num_of_pkts);
	return;
}

unsigned int udp_make_packet(uint8_t pkt_type,uint8_t fin, uint8_t syn,uint8_t ack, unsigned int num_of_pkts,
		unsigned char **pkt, unsigned char *dest_mac, unsigned char *src_mac)
{
	uint8_t firstByte;
	uint8_t host;
	pkt_h *new_pkt_head=NULL;
	unsigned char* new_udp_pkt = NULL, *new_payload=NULL;
	unsigned int pkt_size=0;

#ifdef DEBUG_SATHISH
	char test[100];
	strcpy(test,"sathish");
	print_received_parm(0, 0, pkt_type,ack,syn,fin,num_of_pkts);
#endif

	host=0;
	firstByte = host << 4 | pkt_type << 3 | fin << 2 | syn << 1 | ack;
	if(pkt_type == 0)
		pkt_size=sizeof(pkt_h)+TYPE0_SIZE;
	else
		pkt_size=sizeof(pkt_h)+TYPE1_SIZE;

	new_udp_pkt= (unsigned char *) malloc(pkt_size);

	memset(new_udp_pkt,0,pkt_size);

	new_payload=(unsigned char *)new_udp_pkt + sizeof(pkt_h);
	if (fin)
	{
		dprintf("fin set\n");
		memcpy(new_payload,&num_of_pkts,sizeof(int));
	}
#ifdef DEBUG_SATHISH
	else
	{
		dprintf("entering\n");
		memcpy(new_payload,test,strlen(test));
	}
#endif
	new_pkt_head=(pkt_h *)new_udp_pkt;
	memcpy((char *)new_pkt_head->ether_dhost, (char *)dest_mac, ETHER_ADDR_LEN);
	memcpy((char *)new_pkt_head->ether_shost, (char *)src_mac, ETHER_ADDR_LEN);

	new_pkt_head->first=firstByte;
	*pkt=new_udp_pkt;

#ifdef DEBUG_SATHISH
	print_udp_packet(*pkt);
#endif

	return pkt_size;
}

void print_tcp_packet(unsigned char *pkt)
{
#ifdef DEBUG	
	pkt_h *ptr_to_head=NULL;
	tcp_h *tcp_head=NULL;
	ptr_to_head =(pkt_h *)pkt;
	tcp_head = (tcp_h *) (pkt+sizeof(pkt_h));

	fprintf(stdout,"Packet generated contents\n");
	fprintf(stdout,"Packet Source mac:");
	print_mac_address(ptr_to_head->ether_shost); 
	fprintf(stdout,"\nPacket Destin mac:");
	print_mac_address(ptr_to_head->ether_dhost); 

	fprintf(stdout,"\nFirst byte:%d\n",ptr_to_head->first);
	fprintf(stdout,"Sequence number is:%u\n",tcp_head->seq_no);
	fprintf(stdout,"Payload data is : %s\n",(char *) (pkt+sizeof(pkt_h)+sizeof(tcp_h)));
#endif

	return;
}

unsigned int tcp_make_packet(uint8_t pkt_type,uint8_t fin, uint8_t syn,uint8_t ack,unsigned int seq_num, unsigned int num_of_pkts,
		unsigned char **pkt, unsigned char *dest_mac, unsigned char *src_mac)
{
	uint8_t firstByte;
	uint8_t host;
	pkt_h *new_pkt_head=NULL;
	tcp_h *new_tcp_head=NULL;
	unsigned char *new_tcp_pkt=NULL,*new_payload=NULL;
	unsigned int pkt_size=0;


	//#ifdef DEBUG_SATHISH
	char test[100];
	strcpy(test,"sathish");
	//print_received_parm(host,net,pkt_type,ack,syn,fin,num_of_pkts);
	//#endif

	host=0;
	firstByte = host << 4 | pkt_type << 3 | fin << 2 | syn << 1 | ack;
	if(pkt_type == 0)
		pkt_size=sizeof(pkt_h)+sizeof(tcp_h)+TYPE0_SIZE;
	else
		pkt_size =sizeof(pkt_h)+sizeof(tcp_h)+TYPE1_SIZE;

	new_tcp_pkt= (unsigned char *)  malloc(pkt_size);
	memset(new_tcp_pkt,0,pkt_size);
	new_payload=new_tcp_pkt+sizeof(pkt_h)+sizeof(tcp_h);

	if (fin)
	{
		dprintf("fin set\n");
		memcpy(new_payload,&num_of_pkts,sizeof(unsigned int));
	}
	//#ifdef DEBUG_SATHISH
	else
	{
		dprintf("entering\n");
		memcpy(new_payload,test,strlen(test));
	}
	//#endif

	new_tcp_head=(tcp_h *)(new_tcp_pkt+sizeof(pkt_h));
	new_tcp_head->seq_no=seq_num;

	new_pkt_head=(pkt_h *)new_tcp_pkt;
	memcpy((char *)new_pkt_head->ether_dhost,dest_mac,ETHER_ADDR_LEN);
	memcpy((char *)new_pkt_head->ether_shost,src_mac,ETHER_ADDR_LEN);
	new_pkt_head->first=firstByte;

	*pkt=new_tcp_pkt;

	print_tcp_packet(*pkt);
	return pkt_size;
}

uint16_t calculate_checksum (uint16_t *addr, uint16_t len)
{
	int nleft = len;
	int sum = 0;
	uint16_t *w = addr;
	uint16_t answer = 0;

	while (nleft > 1) {
		sum += *w++;
		nleft -= sizeof (uint16_t);
	}

	if (nleft == 1) {
		*(uint8_t *) (&answer) = *(uint8_t *) w;
		sum += answer;
	}

	sum = (sum >> 16) + (sum & 0xFFFF);
	sum += (sum >> 16);
	answer = ~sum;
	return (answer);
}

void populateMAC(char *dev, unsigned char *macaddr) { 

	/* Assuming the global Variable  \
	   to list of interfaces to be defined */

	dprintf("In populateMAC\n");

	struct ifreq ifr;

	int sendsd;

	/*  creating the raw socket to query for the Interface info */
	if ((sendsd = socket (AF_INET, SOCK_RAW, htons(ETH_P_ALL)) ) < 0) {
		perror ("socket() failed to get socket descriptor for using ioctl() ");
		exit (0);
	}


	memset (&ifr, 0, sizeof (ifr));
	strcpy(ifr.ifr_name,dev);

	if (ioctl (sendsd, SIOCGIFHWADDR, &ifr) < 0) 
	{
		perror ("ioctl() failed to get source MAC address ");
		exit(0);
	}

	if (ifr.ifr_hwaddr.sa_family!=ARPHRD_ETHER) {
		fprintf(stderr, "Not and ethernet interface\n");
		exit(0);
	}

	memset(macaddr, 0, 6);
	memcpy(macaddr, &ifr.ifr_hwaddr.sa_data , 6);

	close(sendsd);

}

void getMacForPcapFilter(char *node,char *mac,session_t *pSession)
{
	unsigned char addr[ETHER_ADDR_LEN];
	char buffer[MAX_NAME_LENGTH];
	strcpy(buffer,"");
	strcpy(mac,"");
	lookMacAddr((char*)node,addr,pSession);
	int i =0;
	for(;i<ETHER_ADDR_LEN;i++)
	{
		if(i<ETHER_ADDR_LEN-1)
			sprintf(buffer,"%02x:", *(addr +i));
		else
			sprintf(buffer, "%02x", *(addr +i));
		strcat(mac,buffer);
	}
	dprintf("Mac generated %s\n",mac);
	return;
}

void print_mac_address(const unsigned char *addr){

#ifdef DEBUG	
	int i =0;
	for(;i<ETHER_ADDR_LEN;i++)
		if(i<ETHER_ADDR_LEN-1)
			fprintf(stdout, "%02x::", *(addr +i));
		else
			fprintf(stdout, "%02x\n", *(addr +i));	
#endif

}

/*void lookMacAddr(char *name, unsigned char *addr)
{
	unsigned char dst_mac[6];

	 // Set destination MAC address: you need to fill these out
	  dst_mac[0] = 0x00;
	  dst_mac[1] = 0x00;
	  dst_mac[2] = 0x00;
	  dst_mac[3] = 0x00;
	  dst_mac[4] = 0x00;
	  dst_mac[5] = 0x04;

	  memcpy(addr, dst_mac, 6);
}*/


void fillMacTable(session_t *pSession)
{
	int i=0,j=0;
	char node_id[2];
	pSession->num_of_mac_entries=0;

	strcpy((char *)pSession->macTable[i].nodeName,"n-0");
	for (j=0;j<ETHER_ADDR_LEN-1;j++)
		pSession->macTable[i].macAddr[j]=0;
	pSession->macTable[i].macAddr[j]=5;
	pSession->num_of_mac_entries++;

	for(i=1;i<=NUM_OF_NODES;i++)
	{
		strcpy((char *)pSession->macTable[i].nodeName,"n-");
		memset(node_id,0,2);
		sprintf(node_id,"%d",i);
		strcat((char *)pSession->macTable[i].nodeName,node_id);

		for (j=0;j<ETHER_ADDR_LEN-1;j++)
			pSession->macTable[i].macAddr[j]=0;

		pSession->macTable[i].macAddr[j]=i;
		pSession->num_of_mac_entries++;
	}	
	dprintf ("Before returning: %d\n", pSession->num_of_mac_entries);
	return;
}

int lookMacAddr(char *node,unsigned char *macAddr,session_t *pSession)
{

	unsigned  i=0;
	int result=-1;
	dprintf("Node passed: %s Entries: %d\n", node, pSession->num_of_mac_entries);
	for (i=0;i<pSession->num_of_mac_entries;i++)
	{
		dprintf("%s\n", pSession->macTable[i].nodeName);
		if(strcmp(node,(char *)pSession->macTable[i].nodeName)==0)
		{
			memcpy(macAddr,pSession->macTable[i].macAddr,ETHER_ADDR_LEN);
			result=0;
		}
	}
	return result;
}
