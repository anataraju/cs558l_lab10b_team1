// SMALL WORLD TCP / UDP

#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <pcap.h>
#include <netinet/if_ether.h>

//#define RTT_MEASURE
//#define DEBUG
//#define DEBUG_SATHISH
#define HACKED
#ifdef DEBUG
	#define dprintf(...) {fprintf(stdout, "%s (%d): ", __FILE__, __LINE__); fprintf(stdout, __VA_ARGS__);}
#else
	#define dprintf(...) {}//do nothing)
#endif

#define NUM_OF_NODES 4
#define MAX_NAME_LENGTH 256

#define HOST_PART(ip) ((ip->first) & 0xC0)
#define NW_PART(ip) ((ip->first) & 0x30)
#define PACKET_TYPE(ip) ((ip->first) & 0x08)
#define FLAGS(ip) ((ip->first) & 0x07)
#define IS_ACK(ip)   ((ip->first) & 0x01)
#define IS_SYN(ip)   ((ip->first) & 0x02)
#define IS_FIN(ip)   ((ip->first) & 0x04)
#define GET_NW_ADDR(ip)     (((ip->first) & 0x30) >> 4)
#define GET_HOST_ADDR(ip)   (((ip->first) & 0xC0) >> 6)
#define IS_LARGE_PKT(ip)    (((ip->first) & 0x08) >> 3)

#define SENDER_MODE 1
#define RECEIVER_MODE   0
#define UDP_PROTO    1
#define TCP_PROTO    0
#define SMALL_PACKET 0
#define NORMAL_PACKET 1
#define TYPE0_SIZE 100
#define TYPE1_SIZE 1490
#define SNAP_LEN 1518
/*typedef unsigned char uint8_t;
typedef unsigned short uint16_t;
typedef unsigned int uint32_t;*/

//#define TCP_TIMEOUT_SLEEP 10000
//#define TCP_SENDER_NUM_COPIES 4
//#define TCP_SENDER_NUM_COPIES_RETRANS 10
#define TCP_SLEEP_AFTER 10
#define TCP_INTERPKT_DELAY 10
//#define TCP_INTERPKT_DELAY_DUPS 2
//#define TCP_INTERPKT_DELAY_RETRANS 10

/**/
#define SINGLE_ACK_PART
#define TCP_TIMEOUT_SLEEP 5000 // frequency at which receiver sends NAKs
//#define TCP_SENDER_NUM_COPIES 1 // openflow-Amogh
//#define TCP_SLEEP_AFTER 100
/**/
/*
#define MULTI_ACK_PART
#define TCP_TIMEOUT_SLEEP 1000000 // frequency at which receiver sends NAKs
#define TCP_SENDER_NUM_COPIES 1 // openflow-Amogh
#define TCP_SLEEP_AFTER 1000
*/

#if 0
	unsigned char host:2; /* Host address */
	unsigned char network:2; /* Network address */
	unsigned char   packet_type:1; /* 1 - 1500 Bytes, 0 - 100 Bytes */
	unsigned char   flags:3; /* 1-ACK, 2-SYN, 4-FIN */  // Note: FIN is used by UDP as well
	// For now SYN is not being used
#endif 

typedef struct packet_header
{
	 	unsigned char  ether_dhost[ETHER_ADDR_LEN];    /* destination host address */
        unsigned char  ether_shost[ETHER_ADDR_LEN];    /* source host address */
		unsigned char first;
	//uint16_t    checksum;
}pkt_h;

// xx101xxx
// if (((first & 0x28) >> 3) == 5)

/* specific to TCP */
typedef struct tcp_header {
	uint32_t seq_no;
	//uint32_t ack_no;
}tcp_h;

enum TCP_STATE {
	INITIAL = 0, SYN_SENT, CONNECTED, FIN_SENT, CLOSED
};

/*	
pair<char*, char[6]> mac_entry_type;
map<char*, char[6]> map_mac_addr;
map<char*, char[6]>::iterator it_type;
*/
	
typedef struct table
{
	unsigned char nodeName[MAX_NAME_LENGTH];
	unsigned char macAddr[ETHER_ADDR_LEN];
}macLookupTable;

// Parse command line and populate, rest should be maintained as per progress
typedef struct session {
	unsigned int protocol; // tcp - 0 or udp - 1
	unsigned int numPkts;    // packets to send
	unsigned int numPktsSent;
	unsigned int duration; // seconds
	unsigned int packetSize; // 0 - 100 Bytes, 1 - 1500 Bytes
	unsigned int cur_seq_no; // keep track of sequence number in tcp alone
	char receiverName[MAX_NAME_LENGTH];
	char senderName[MAX_NAME_LENGTH];
	unsigned char receiverMac[ETHER_ADDR_LEN];
	unsigned char senderMac[ETHER_ADDR_LEN];
	unsigned int mode;   // 0 - sender, 1 - receiver
	macLookupTable macTable[NUM_OF_NODES];
	// if protocol - 1
	int tcp_state;  // INITIAL, SYN_SENT, CONNECTED, FIN_SENT, CLOSED
	unsigned int num_of_mac_entries;

}session_t;

void getMacForPcapFilter(char*node,char *mac,session_t *pSession);
// Amogh
int parse_cmdline(int argc, char * argv[], session_t *pSession);       // called from main

// UDP Sender
// Venkat
void udp_send(session_t *pSession);
void print_udp_packet(unsigned char *pkt);
void print_tcp_packet(unsigned char *pkt);
/*
- open connection for sending packet
- for numPackets or duration
-   make packets and send
-   keep counting if duration is given
- send last packet 10 times
- exit? 
 */

// Sathish
//void udp_make_packet(int size, unsigned char *pkt);
unsigned int udp_make_packet(uint8_t pkt_type,uint8_t fin, uint8_t syn,uint8_t ack, unsigned int num_of_pkts,unsigned char **pkt, unsigned char *dest, unsigned char *src);


// Ankit 
// UDP receiver
void udp_receiver(session_t *pSession);
/* 
while (1) {
    pcap sniff
    if (our protocol)
        if (last packet) {
            //set flag - last packet
            goto lastPacket;
        }
        keep listening

    lastPacket:
    print statistics
}
exit 
 */

// Sathish
// TCP Sender
//unsigned int tcp_make_packet(uint8_t host,uint8_t net,uint8_t pkt_type,uint8_t fin, uint8_t syn,uint8_t ack, unsigned int seq_num, unsigned int num_of_pkts,unsigned char **pkt);
unsigned int tcp_make_packet(uint8_t pkt_type,uint8_t fin, uint8_t syn,uint8_t ack,unsigned int seq_num, unsigned int num_of_pkts, 
		unsigned char **pkt, unsigned char *dest_mac, unsigned char *src_mac);
		
// Manoj
// TCP receiver
// INITIAL, SYN_SENT, CONNECTED, FIN_SENT, CLOSED
// Thread function to receive the TCP like packets
void tcp_pkt_receiver(session_t *pSession);
/* 
while (1) {
    pcap sniff


    if (our protocol and protocol == tcp)
        if (last packet) {
            //set flag - last packet
            goto lastPacket;
        }
        keep listening

    lastPacket:
    print statistics
}
exit 
 */


// Watchdog thread to keep sending the NAKs every X millisecs 
// Sync between tcp_pkt_receiver using mutex
// Amogh
void * tcp_nak_sender();
void tcp_pkt_sender(session_t *pSession);



// Tomorrow's merge & conquer
// Sathish & Lakshmi

void send_first_packet(session_t *pSession, pcap_t *handle);
void send_last_packet(session_t *pSession, pcap_t *handle);
void send_normal_data(session_t *pSession, pcap_t *handle);
void populateMAC(char *, unsigned char *macaddr);
void print_mac_address(const unsigned char *addr);
//void lookMacAddr(char *name, unsigned char *addr);
int lookMacAddr(char *node,unsigned char *macAddr,session_t *pSession);
void fillMacTable(session_t *pSession);

void printSessionValues(session_t *pSession);
void printMacTableEntries(session_t *pSession);
