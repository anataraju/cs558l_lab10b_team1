/*
 * tcp_receiver.h
 *
 */

#include <vector>
using namespace std;

#ifndef TCP_RECEIVER_H_
#define TCP_RECEIVER_H_

#define IDLE_STATE 		0
#define SYN_RECEIVED	1
#define ESTABLISHED		2
#define FIN_RECEIVED	4
#define CLOSED			8

#define SECTOUSEC		1000000
#define DUP_SYN_ACKS	10

void* rcvrThread (void *arg);
void myCallback(u_char* arg, const struct pcap_pkthdr *pkthdr,const u_char* packet);
int isPacketPresent(List* list, unsigned int id);
int InsertIntoList(List *list, void *element);
int CompareTransElem(void *src, void *dst);
void* rvcrTimeout (void *arg);
void printStatisticsT(void *);

typedef struct rcvrSession {
        session_t *ses;
        pcap_t* sniffHandle;
        pcap_t* sniffWriteHandle1;
        pcap_t* sniffWriteHandle2;
        vector <bool> *rcvrList;
        uint32_t expected_seqno;
        uint32_t inOrderPackets;
        uint32_t packetsAddedToList;
        unsigned char src_mac[ETHER_ADDR_LEN];
        unsigned char des_mac[ETHER_ADDR_LEN];

} rcvrSession_t;

#endif /* TCP_RECEIVER_H_ */
