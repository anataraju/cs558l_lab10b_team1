#include <stdio.h>
#include <pcap.h>
#include "common.h"
#include <string.h>
#include <signal.h>
#include <time.h>
#include <unistd.h>

#define NO_OF_PACKETS 10
#define SLEEP_TIME 10
#define NO_OF_DUP 40

struct itimerval itv;
pcap_t *handle;
session_t *sess_data;
unsigned char udps_src_mac[6];
unsigned char udps_dst_mac[6];


//Signal handler for SIGALRM
static void sigalrm(int sig)
{
	//send the last packet
	send_last_packet(sess_data, handle);

	exit(0);
}	

void send_first_packet(session_t *pSession, pcap_t *handle)
{
	unsigned char *pkt=NULL;
	int i=0;
	unsigned int pkt_size=0;
	if (pSession->packetSize == TYPE0_SIZE )
		pkt_size=udp_make_packet(0,0,1,0,pSession->numPktsSent,&pkt,  udps_dst_mac, udps_src_mac);
	else
		pkt_size=udp_make_packet(1,0,1,0,pSession->numPktsSent,&pkt,  udps_dst_mac, udps_src_mac);

	dprintf("reached here!! Packet size->%d\n", pkt_size);

#ifdef DEBUG_SATHISH
	print_udp_packet(pkt);
#endif
	for(i=0;i<NO_OF_DUP;i++)
	{
		dprintf("Just before sending the packet\n");

		if (pcap_inject(handle,pkt,pkt_size)==-1)
		{
			pcap_perror(handle,0);
			pcap_close(handle);
			exit(1);
		}
	}
	free(pkt);
	return;
}

void send_last_packet(session_t *pSession, pcap_t *handle)
{
	unsigned char *pkt=NULL;
	int i=0;
	unsigned int pkt_size=0;
	if (pSession->packetSize == 100 )
		pkt_size=udp_make_packet(0,1,0,0,pSession->numPktsSent,&pkt,  udps_dst_mac, udps_src_mac);
	else
		pkt_size=udp_make_packet(1,1,0,0,pSession->numPktsSent,&pkt,  udps_dst_mac, udps_src_mac);

#ifdef DEBUG_SATHISH
	print_udp_packet(pkt);
#endif

	for(i=0;i<NO_OF_DUP;i++)
	{
		if (pcap_inject(handle,pkt,pkt_size)==-1)
		{
			pcap_perror(handle,0);
			pcap_close(handle);
			exit(1);
		}
	}
	free(pkt);
	return;
}

void send_normal_data(session_t *pSession, pcap_t *handle)
{
	struct sigaction sact;
	//time_t t;

	sigemptyset(&sact.sa_mask);
	sact.sa_flags = 0;
	sact.sa_handler = sigalrm;
	sigaction(SIGALRM, &sact, NULL);

	alarm(pSession->duration); /* timer will pop after these many seconds */

	unsigned char *pkt=NULL;
	unsigned int pkt_size=0;//time_int=0;
	int j=0;
	/*struct timeval start, end;
	gettimeofday(&start, NULL);*/

	while (1)
	{
		//If no. of packets reached before 10 seconds, return
		if(pSession->numPkts > 0)
		{
			if(pSession->numPktsSent >= pSession->numPkts)
			{
				return;
			}	
		}

		if (pSession->packetSize == 100 )
			pkt_size=udp_make_packet(0,0,0,0,pSession->numPktsSent,&pkt, udps_dst_mac, udps_src_mac);
		else
			pkt_size=udp_make_packet(1,0,0,0,pSession->numPktsSent,&pkt, udps_dst_mac, udps_src_mac);

#ifdef DEBUG_SATHISH
		print_udp_packet(pkt);
#endif

		pSession->numPktsSent++;
		if (pcap_inject(handle,pkt,pkt_size)==-1)
		{
			pcap_perror(handle,0);
			pcap_close(handle);
			exit(1);
		}

		/*gettimeofday(&end, NULL);
		time_int=(end.tv_sec * 1000000 + end.tv_usec)-(start.tv_sec * 1000000 + start.tv_usec);

		if(time_int >= pSession->duration)
			break;
		 */
		j++;
		if(j == NO_OF_PACKETS)
		{
			j=0;
			usleep(SLEEP_TIME);
		}
	}
	return;
}

/*

- open connection for sending packet
- for numPackets or duration
- 	make packets and send
-	keep counting if duration is given
- send last packet 10 times
- exit?
 */

void udp_send(session_t *pSession)
{

	char *dev, errbuf[PCAP_ERRBUF_SIZE];


	dev= (char *) malloc(MAX_NAME_LENGTH);
	/*dev = pcap_lookupdev(errbuf);
	if (dev == NULL)
	{
		fprintf(stderr,"Couldn't find default device: %s\n", errbuf);
		exit(0);
	}*/

	strcpy(dev,"inf000");
	populateMAC(dev, udps_src_mac);

	dprintf("device name is %s, MAC address is",dev);
	print_mac_address(udps_src_mac);


	//get dest mac address
	if(lookMacAddr(pSession->receiverName, udps_dst_mac, pSession) == -1)
	{	
		fprintf(stderr, "Dest mac lookup failed!!!");
		exit(-1);
	}	

	dprintf("Dest MAC address is: ");
	print_mac_address(udps_dst_mac);

	char getMac[MAX_NAME_LENGTH];
	char filter_exp[] = "ether src ";		/* filter expression [3] */
	getMacForPcapFilter(pSession->receiverName,getMac,pSession);
	strcat(filter_exp,getMac);
	dprintf ("Filter expression:%s\n",filter_exp);

	struct bpf_program fp;
	bpf_u_int32 mask;
	bpf_u_int32 net;

	if (pcap_lookupnet(dev, &net, &mask, errbuf) == -1)
	{
		fprintf(stderr, "Couldn't get netmask for device %s: %s\n",dev, errbuf);
		net = 0;
		mask = 0;
	}

	handle= pcap_open_live(dev, BUFSIZ, 1, 1000, errbuf);
	if(handle == NULL)
	{
		fprintf(stderr,"pcap open live failed\n");
		exit(0);
	}
	else
		dprintf("pcap open live success\n");
	//open connection

	if(pcap_setdirection(handle, PCAP_D_IN) == -1)
	{
		fprintf(stderr, "Couldn't set direction for device %s\n", dev);
		exit(-1);
	}

	// compiling the filter expression
	if (pcap_compile(handle, &fp, filter_exp, 0, net) == -1)
	{
		fprintf(stderr, "Couldn't parse filter %s: %s\n",filter_exp, pcap_geterr(handle));
		exit(0);
	}


	// applying the filter
	if (pcap_setfilter(handle, &fp) == -1)
	{
		fprintf(stderr, "Couldn't install filter %s: %s\n",filter_exp, pcap_geterr(handle));
		exit(0);
	}

	memset(&sess_data, 0, sizeof(sess_data));
	//memcpy((unsigned char *)&sess_data,  (unsigned char *)pSession, sizeof(sess_data));
	sess_data = pSession;

	send_first_packet(pSession, handle);
	send_normal_data(pSession, handle);
	send_last_packet(pSession, handle);

	free(dev);
	return;
}
