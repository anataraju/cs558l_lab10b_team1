all: iperf

iperf: iperf.o common.o list.o udp_sender.o udp_receiver.o tcp_sender.o tcp_receiver.o
	g++ -o iperf -g iperf.o list.o common.o udp_sender.o udp_receiver.o tcp_sender.o tcp_receiver.o -lpcap -pthread
#	g++ -o iperf -g iperf.o tcpsender.o tcpreceiver.o udpsender.o udpreceiver.o list.o -lpcap -pthread

iperf.o: iperf.cpp 
	g++ -g -c -Wall iperf.cpp

common.o: common.cpp
	g++ -g -c -Wall common.cpp

tcp_sender.o: tcp_sender.cpp
	g++ -g -c -Wall tcp_sender.cpp -lpcap -pthread

tcp_receiver.o: tcp_receiver.cpp
	g++ -g -c -Wall tcp_receiver.cpp -lpcap -pthread

udp_sender.o: udp_sender.cpp
	g++ -g -c -Wall udp_sender.cpp -lpcap 

udp_receiver.o: udp_receiver.cpp
	g++ -g -c -Wall udp_receiver.cpp -lpcap -pthread

list.o: list.cpp
	g++ -g -c -Wall list.cpp
	
clean:
	rm -f *.o iperf
