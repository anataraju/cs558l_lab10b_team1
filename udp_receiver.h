/*
 * udp_receiver.h
 *
 */

#ifndef UDP_RECEIVER_H_
#define UDP_RECEIVER_H_


#define NO_OF_DUP		10
#define IDLE_STATE 		0
#define SYN_RECEIVED	1
#define ESTABLISHED		2
#define FIN_RECEIVED	4
#define CLOSED			8

#define SECTOUSEC		1000000

void udpReceiver(session_t *sess);
void packetSniffer(char *dev, session_t *sess);
void my_callback(u_char *pses, const struct pcap_pkthdr* pkthdr,const u_char* packet);
void printStatistics();


#endif /* UDP_RECEIVER_H_ */
